
import kinawat.TextIO;

/**
 * @author Jeremy Patrick
 */
public class SimpleExpressionEvaluator {

    public static void main(String[] args) {
        double num1, num2, out = 0;
        char operator;

        do {
            System.out.print("Enter expression: ");
            num1 = TextIO.getDouble();
            operator = TextIO.getChar();
            num2 = TextIO.getDouble();
            switch (operator) {
                case '+':
                    out = num1 + num2;
                    break;
                case '-':
                    out = num1 - num2;
                    break;
                case '*':
                    out = num1 * num2;
                    break;
                case '/':
                    out = num1 / num2; // unhandled DivisionWithZero exception, num1 / 0
                    break;
            }

            System.out.println(num1 + " " + operator + " " + num2 + " = " + out);
        } while (num1 != 0);
    }
}

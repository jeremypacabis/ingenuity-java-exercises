
import java.util.Scanner;


/**
 * @author Jeremy Patrick
 */
public class LineBreaker {

    public static void main(String[] args) {
        System.out.print("Enter sentence to be broken: ");
        for (String word : new Scanner(System.in).nextLine().replaceAll("[^A-Za-z0-9]", " ").split("\\s+")) {
            System.out.println(word);
        }
    }

}


import java.util.Random;

/**
 * @author Jeremy Patrick
 */
public class RollDice {
    public static void main(String[] args) {
        int roll1 = new Random().nextInt(5) + 1;
        int roll2 = new Random().nextInt(5) + 1;
        System.out.println("The first die comes up " + roll1 + "\nThe second die comes up " + roll2 + "\nYour total roll is " + (roll1 + roll2));
    }
}


/**
 * @author Jeremy Patrick
 */
public class Divisors {

    public static void main(String[] args) {
        int x = 1;
        int currentNum = 0;
        int numOfDivisors = 0;
        int tempDivisors;
        for (; x <= 10000; x++) {
            tempDivisors = 0;
            for (int z = 1; z <= x; z++) {
                if (x % z == 0) {
                    tempDivisors++;
                }
            }
            numOfDivisors = tempDivisors >= numOfDivisors ? tempDivisors : numOfDivisors;
            currentNum = tempDivisors >= numOfDivisors ? x : currentNum;
        }

        System.out.println("The number which has the largest number of divisors is " + currentNum + " with " + numOfDivisors + " divisors.");
    }
}

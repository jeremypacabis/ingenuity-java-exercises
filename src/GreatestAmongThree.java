
import java.util.Scanner;

/**
 * @author Jeremy Patrick
 */
public class GreatestAmongThree {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int num1, num2, num3;
        try {
            System.out.print("Enter number 1: ");
            num1 = scanner.nextInt();
            System.out.print("Enter number 2: ");
            num2 = scanner.nextInt();
            System.out.print("Enter number 3: ");
            num3 = scanner.nextInt();
            System.out.println("The gretest among " + num1 + ", " + num2 + " and " + num3 + " is " + (num1 > num2 ? (num1 > num3 ? num1 : num3) : (num2 > num3 ? num2 : num3)) + ".");
        } catch (NumberFormatException nfe) {
            System.err.println("Invalid number");
        }
    }
}

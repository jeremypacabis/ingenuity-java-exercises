
import java.util.Random;

/**
 * @author Jeremy Patrick
 */
public class SnakeEyes {

    public static void main(String[] args) {
        int rolls = 0;
        int roll1, roll2;

        do {
            rolls++;
            roll1 = new Random().nextInt(6) + 1;
            roll2 = new Random().nextInt(6) + 1;
            System.out.println("Roll #" + rolls + ": " + roll1 + " | " + roll2);
        } while (roll1 + roll2 != 2);

        System.out.println("Snake eyes come up after " + rolls + " rolls.");
    }
}

package jeremypacabis.shapes;

/**
 * @author Jeremy Patrick
 */
public class Shape {

    protected String color;
    protected boolean filled;

    public Shape() {
    }

    public Shape(String color, boolean filled) {
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
    }

    public boolean isFilled() {
        return filled;
    }

    public void setFilled(boolean filled) {
    }

    public double getArea() {
        return 0;
    }

    public double getPerimeter() {
        return 0;
    }

    public double getRadius() {
        return 0;
    }

    public double getLength() {
        return 0;
    }

    public double getSide() {
        return 0;
    }
}

package jeremypacabis.shapes;

/**
 * @author Jeremy Patrick
 */
public interface Resizable {
    public void resizeShape(double updateValue);
}

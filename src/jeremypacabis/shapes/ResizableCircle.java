package jeremypacabis.shapes;

/**
 * @author Jeremy Patrick
 */
public class ResizableCircle extends Shape implements Resizable {

    protected double radius;

    public ResizableCircle() {
    }

    public ResizableCircle(double radius, String color, boolean filled) {
        this.radius = radius;
        this.color = color;
        this.filled = filled;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public double getArea() {
        return Math.PI * radius * radius;
    }

    @Override
    public double getPerimeter() {
        return Math.PI * 2 * radius;
    }

    @Override
    public String toString() {
        return String.valueOf(this);
    }

    @Override
    public void resizeShape(double updateValue) {
        this.radius += updateValue;
    }

}

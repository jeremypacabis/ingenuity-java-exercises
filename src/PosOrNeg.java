
import java.util.Scanner;

public class PosOrNeg {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int input;

        System.out.print("Enter number: ");
        try {
            input = scanner.nextInt();
            System.out.println("Entered number " + input + " is " + (input >= 0 ? "positive." : "negative."));
        } catch (NumberFormatException nfe) {
            System.err.println("Invalid number");
        }
    }
}
